let state = 'view';

const WINNING_SOUNDS = [
  'sounds/freesound.org-WinBandoneon.mp3',
  'sounds/freesound.org-WinBanjo.mp3',
  'sounds/freesound.org-WinBrass.mp3',
  'sounds/freesound.org-WinDoot.mp3',
  'sounds/freesound.org-WinFantasia.mp3',
  'sounds/freesound.org-WinFretless.mp3',
  'sounds/freesound.org-WinGrandPiano.mp3',
  'sounds/freesound.org-WinHarpsichord.mp3',
  'sounds/freesound.org-WinMutedGuitar.mp3',
  'sounds/freesound.org-WinSquare.mp3'
];

let winningSound = new Audio(WINNING_SOUNDS[Math.floor(Math.random()*WINNING_SOUNDS.length)]);
console.log(winningSound);

window.addEventListener('load', function() {
  let buzzwords = [];
  if (window.location.hash != "") {
    // load buzzwords from url hash
    buzzwords = JSON.parse(atob(window.location.hash.substring(1)));
  } else {
    // assign random buzzwords
    buzzwords = ['cyber!', 'Blockchain', 'Flugtaxi', '5G', 'innovativ', 'Smart City', 'agil', 'Leuchtturm', 'App', 'Souveränität', 'Irgendwas 4.0'];
  }

  // randomize buzzwords
  buzzwords.sort(() => Math.random() - 0.5);
  buzzwords = buzzwords.slice(0, 9);

  // update buzzword tiles
  if (buzzwords instanceof Array && buzzwords.length == 9) {
    document.querySelectorAll('.card').forEach((item, i) => {
      item.innerText = buzzwords[i];
    });
  }
});

// add click event listener to cards
document.querySelectorAll('.card').forEach((item, i) => {
  item.addEventListener('click', function() {
    if (!(item.hasAttribute('contentEditable') && item.attributes['contentEditable'].value == "true")) {
      item.classList.toggle('active');

      // check if inactive item in horozontal row
      let row = item.parentElement.parentElement;
      let row_incomplete = Array.from(row.children).map(x => x.firstChild.classList.contains('active')).includes(false);

      // check if inactive item in vertical column
      let y_index = Array.from(row.children).map(x => x.firstChild).findIndex(x => x == item);
      let rows = row.parentElement.children;
      let column_incomplete = Array.from(rows).map(x => x.children[y_index].firstChild.classList.contains('active')).includes(false); 

      if (!row_incomplete || !column_incomplete) {
        winningSound.currentTime = 0;
        winningSound.play();
      }
    }
  });
});

document.querySelector('#btnEdit').addEventListener('click', function() {
  switch (state) {
    case 'view':
      // switch to edit mode
      document.querySelector('#btnEdit').innerText = "Save";
      document.querySelectorAll('.card').forEach((item, i) => {
        item.setAttribute('contentEditable', true);
        item.classList.remove('active');
      });
      state = 'edit';
      break;
    case 'edit':
      // switch to view mode
      document.querySelector('#btnEdit').innerText = "Edit";
      let buzzwords = [];
      document.querySelectorAll('.card').forEach((item, i) => {
        item.setAttribute('contentEditable', false);
        buzzwords.push(item.innerText);
      });
      // store card values in url hash
      window.location.hash = btoa(JSON.stringify(buzzwords));
      state = 'view';
      break;
  }
});

if (navigator.clipboard) {
  document.querySelector('#aCopyUrl').addEventListener('click', function() {
    navigator.clipboard.writeText(window.location.href);
  });
} else {
  document.querySelector('#aCopyUrl').remove();
}
